﻿using System;
using System.Collections.Generic;

namespace DataStructures
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            SetData();

            Console.WriteLine("\n--------------");
            foreach (var p in SearchPathToKey("bugs1"))
                Console.Write($"-{p}-");
            Console.WriteLine("\n--------------");
            foreach (var p in SearchPathToKey("Tampletes"))
                Console.Write($"-{p}-");
            SetInExistingNode("bugs1", "ewqewqeqw");
            Console.WriteLine("\n--------------");
            foreach (var p in SearchPathToKey("ewqewqeqw"))
                Console.Write($"-{p}-");
            Console.WriteLine();
            Console.ReadKey();
        }


        #region Tests
        public static void TestAsInterface()
        {
            var a = RawDataDic as IDictionary<string, object>;
            TreeTraversal(a);
        }
        public static void TestAsClass()
        {
            TreeTraversalDic(RawDataDic);
        }
        public static void TestWithSetValueOnTree()
        {
            TreeTraversalDic(TreeTraversalSet(RawDataDic));
        }
        #endregion

        #region Data
        public static Random r = new Random();
        static Dictionary<string, object> RawDataDic = new Dictionary<string, object>();
        public static object Value
        {
            get
            {
                return r.Next(-100, 100);
            }
        }
        public static void SetData()
        {
            RawDataDic = new Dictionary<string, object>()
            {
                ["App1"] = new Dictionary<string, object>()
                {
                    ["Template1"] = new Dictionary<string, object>()
                    {
                        ["users1"] = new List<Dictionary<string, object>>()
                        {
                            new Dictionary<string, object>()
                            {
                                ["user1.1"] = "user1.1!" ,
                                ["user1.2"] = new Dictionary<string, object>()
                                {
                                    ["bugs1.1"] = "bugs1.1!" 
                                }
                            }
                        }
                    }
                },
                ["App2"] = new Dictionary<string, object>()
                {
                    ["Template2"] = new List<Dictionary<string, object>>()
                    {
                        new Dictionary<string, object>()
                        {
                            ["Tampletes2"] = "Template2!"
                        }
                    }
                },
                ["App3"] = new Dictionary<string, object>()
                {
                    ["Template3"] = new List<Dictionary<string, object>>()
                    {
                        new Dictionary<string, object>()
                        {
                            ["users3"] = new List<Dictionary<string, object>>()
                            {
                                new Dictionary<string, object>()
                                {
                                    ["user3.1"] = "user3.1!" ,
                                    ["user3.2"] = new Dictionary<string, object>()
                                    {
                                        ["bugs3.1"] = "bugs3.1!"
                                    }
                                }
                            }
                        }
                    }
                }
            };
        }

        #endregion

        #region Interfaces
        public static void TreeTraversal(IDictionary<string, object> rawData)
        {
            foreach (var kv in rawData)
            {
                if (rawData[kv.Key] is IList<IDictionary<string, object>>)
                {
                    foreach (var child in rawData as IList<IDictionary<string, object>>)
                    {
                        TreeTraversal(child as IDictionary<string, object>);
                    }
                }
                else if (rawData[kv.Key] is IDictionary<string, object>)
                {
                    TreeTraversal(rawData[kv.Key] as IDictionary<string, object>);
                }
                else
                {
                    Console.WriteLine(rawData[kv.Key]);
                }
            }
        }
       
        #endregion

        #region TreeTraversal

        public static void TreeTraversalDic(Dictionary<string, object> rawData)
        {
            foreach (var kv in rawData)
            {
                if (rawData[kv.Key] is List<Dictionary<string, object>>)
                {
                    foreach (var child in rawData[kv.Key] as List<Dictionary<string, object>>)
                    {
                        TreeTraversalDic(child);
                    }
                }
                else if (rawData[kv.Key] is Dictionary<string, object>)
                {
                    TreeTraversalDic(rawData[kv.Key] as Dictionary<string, object>);
                }
                else
                {
                    Console.WriteLine(rawData[kv.Key] != null ? rawData[kv.Key] : "null");
                }
            }
        }
        
        #endregion

        #region SetValueToTree

        public static Dictionary<string, object> TreeTraversalSet(Dictionary<string, object> rawData)
        {
            foreach (var kv in rawData)
            {
                if (rawData[kv.Key] is List<Dictionary<string, object>>)
                {
                    foreach (var target in rawData[kv.Key] as List<Dictionary<string, object>>)
                    {
                        if (target is Dictionary<string, object>)
                        {
                            TreeTraversalSet(target);
                        }
                        else
                        {
                            throw new Exception("Unexpected type (expected Dictionary<string, object>)");
                        }
                    }
                }
                else if (rawData[kv.Key] is Dictionary<string, object>)
                {
                    TreeTraversalSet(rawData[kv.Key] as Dictionary<string, object>);
                }
                else
                {
                    rawData[kv.Key] = new Dictionary<string, object>()
                    {
                        [Value.ToString()] = Value
                    };
                }
            }
            return rawData;
        }

        #endregion

        #region SetValueInExistingNode

        public static void SetInExistingNode(string key, string property)
        {
            var path = SearchPathToKey(key);
            SetInExistingNode(path, property, RawDataDic, 0);
        }
        private static void SetInExistingNode(List<string> path, string property, Dictionary<string, object> node, int indexPath)
        {
            if (node.ContainsKey(path[indexPath]))
            {
                if (node[path[indexPath]] is Dictionary<string, object>)
                {
                    SetInExistingNode(path, property, node[path[indexPath]] as Dictionary<string, object>, indexPath + 1);
                }
                else if (node[path[indexPath]] is List<Dictionary<string, object>>)
                {
                    foreach (var child in node[path[indexPath]] as List<Dictionary<string, object>>)
                    {
                        SetInExistingNode(path, property, child as Dictionary<string, object>, indexPath + 1);
                    }
                }
                else
                {
                    node[path[indexPath]] = new Dictionary<string, object>()
                    {
                        [property] = null
                    };
                    return;
                }
            }
        }

        #endregion

        #region Searching

        private static bool IsPathFinded;
        private static List<string> SearchPathToKey(string key)
        {
            IsPathFinded = false;
            return SearchPathToKey(key, new List<string>(), RawDataDic);
        }

        private static List<string> SearchPathToKey(string key, List<string> path, Dictionary<string, object> step)
        {
            if (step == null)
            {
                throw new Exception("Searching in null properties");
            }

            foreach (var kv in step)
            {
                if (step[kv.Key] is Dictionary<string, object>)
                {
                    if (kv.Key == key)
                    {
                        path.Add(key);
                        IsPathFinded = true;
                        return path;
                    }
                    path.Add(kv.Key);
                    SearchPathToKey(key, path, step[kv.Key] as Dictionary<string, object>);
                    if (IsPathFinded)
                    {
                        return path;
                    }
                }
                else if (step[kv.Key] is List<Dictionary<string, object>>)
                {
                    if (kv.Key == key)
                    {
                        path.Add(key);
                        IsPathFinded = true;
                        return path;
                    }
                    foreach (var child in step[kv.Key] as List<Dictionary<string, object>>)
                    {
                        path.Add(kv.Key);
                        SearchPathToKey(key, path, child as Dictionary<string, object>);
                        if (IsPathFinded)
                        {
                            return path;
                        }
                    }
                }
                else
                {
                    if (kv.Key == key)
                    {
                        path.Add(key);
                        IsPathFinded = true;
                        return path;
                    }
                }
            }
            IsPathFinded = false;
            return path;
        }

        #endregion
    }
}


